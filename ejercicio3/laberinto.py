# -*- coding: utf-8 -*-
import re


class Celda(object):
    # No es indispensable, pero despues la legibilidad es mas mejor IMO.
    def __init__(self, pared_izq: bool, pared_arriba: bool, pared_der: bool, pared_abajo: bool):
        self.pared_izq = pared_izq
        self.pared_arriba = pared_arriba
        self.pared_der = pared_der
        self.pared_abajo = pared_abajo

        self.visitada = False
        self.caminoActual = False

    @property
    def paredes(self):
        return [self.pared_izq, self.pared_arriba, self.pared_der, self.pared_abajo]

    @property
    def info(self):
        return {"visitada": self.visitada, "caminoActual": self.caminoActual}


class Laberinto(object):
    def __init__(self, parent=None):
        self.parent = parent

        self._dim_i = 0
        self._dim_j = 0
        self._filas = []

        self._pos_rata = (0, 0)
        self._pos_queso = (0, 0)

    def cargar(self, fn):
        f = open(fn, "r")
        plain_txt = f.read().split("\n")
        f.close()

        # Dimensiones
        re_dim = re.search(r"Dim\(([0-9]*),([0-9]*)\)", plain_txt[0])
        self._dim_i, self._dim_j = int(re_dim.group(1)), int(re_dim.group(2))

        # Filas
        filas = []
        for line in plain_txt[1:-1]:
            re_cols = re.findall(r"\[([0-9]),([0-9]),([0-9]),([0-9])\]", line)
            filas.append([Celda(*self._parseCelda(*paredes)) for paredes in re_cols])
        self._filas = filas

        self.resetear()

    def tamano(self):
        return self._dim_i, self._dim_j

    def resetear(self):
        self._pos_rata = (0, 0)
        self._pos_queso = (self._dim_i - 1, self._dim_j - 1)

        for fila in self._filas:
            for celda in fila:
                celda.visitada = False
                celda.caminoActual = False

    def setPosicionRata(self, i, j):
        if self._enLaberinto(i, j):
            self._pos_rata = (i, j)
            return True
        return False

    def getPosicionRata(self):
        return self._pos_rata

    def esPosicionRata(self, i, j):
        return self._pos_rata == (i, j)

    def setPosicionQueso(self, i, j):
        if self._enLaberinto(i, j):
            self._pos_queso = (i, j)
            return True
        return False

    def getPosicionQueso(self):
        return self._pos_queso

    def esPosicionQueso(self, i, j):
        return self._pos_queso == (i, j)

    def get(self, i, j):
        return self._filas[i][j].paredes

    def getInfoCelda(self, i, j):
        return self._filas[i][j].info

    def resuelto(self):
        return self._pos_rata == self._pos_queso

    def resolver(self):
        """Iniciar la búsqueda de soluciones para el laberinto"""
        celda_rata = self._getCelda(*self.getPosicionRata())
        celda_rata.visitada = True
        celda_rata.caminoActual = True

        return self._run()

    def _direccionesPosibles(self):
        pos_rata = self.getPosicionRata()

        return (self._esDestinoPosible(*aIzquierda(*pos_rata)),
                self._esDestinoPosible(*aArriba(*pos_rata)),
                self._esDestinoPosible(*aDerecha(*pos_rata)),
                self._esDestinoPosible(*aAbajo(*pos_rata)))

    def _esDestinoPosible(self, to_i, to_j):
        """Recibe coordenadas (i,j) de destino y devuelve True si es posible moverse a esa celda, False de lo contrario."""
        # Fuera del laberinto
        if not self._enLaberinto(to_i, to_j):
            return False

        from_i, from_j = self.getPosicionRata()
        to_celda = self._getCelda(to_i, to_j)

        # Se intentó mover a una celda destino con dist > 1
        if from_i - to_i + from_j - to_j > 1:
            return False

        # Comprobar si en la celda actual hay pared en la dirección que se quiere avanzar.
        coords = (from_i, from_j, to_i, to_j)
        hay_pared = ((esIzquierda(*coords) and to_celda.pared_izq)
                     or (esDerecha(*coords) and to_celda.pared_der)
                     or (esArriba(*coords) and to_celda.pared_arriba)
                     or (esAbajo(*coords) and to_celda.pared_abajo))

        if hay_pared:
            return False

        # Ya recorrido
        if to_celda.visitada or to_celda.caminoActual:
            return False

        return True

    def _run(self):
        """Buscar soluciones para el laberinto por backtracking"""
        camino_actual = []
        i_inicial, j_inicial = self.getPosicionRata()
        resuelto, imposible = self.resuelto(), False

        while not resuelto and not imposible:
            from_i, from_j = self.getPosicionRata()
            from_celda = self._getCelda(from_i, from_j)

            # Direcciones posibles
            izq, arriba, der, abajo = self._direccionesPosibles()

            if der:
                to_i, to_j = aDerecha(from_i, from_j)
            elif abajo:
                to_i, to_j = aAbajo(from_i, from_j)
            elif izq:
                to_i, to_j = aIzquierda(from_i, from_j)
            elif arriba:
                to_i, to_j = aArriba(from_i, from_j)

            if izq or arriba or der or abajo:
                # Hay al menos una direccion posible
                # Mover la rata ahi
                self.setPosicionRata(to_i, to_j)

                # Setear como visitada y parte del camino actual
                to_celda = self._getCelda(to_i, to_j)
                to_celda.visitada, to_celda.caminoActual = True, True

                self._redibujar()

                # Agregar a la lista de posiciones del camino actual y chequear si se resolvió
                camino_actual += [(from_i, from_j)]
                resuelto = self.resuelto()
            else:
                print("HORA DE LA BACKTRACKEADA")
                # Inicializar
                from_celda.caminoActual = False
                dirs_posibles = (False, False, False, False)

                # Volver hasta una posicion con movimientos disponibles.
                # Si no existe, terminar cuando se vuelva a la posición de inicio y no queden más direcciones disponibles.
                while not (from_i == i_inicial and from_j == j_inicial) and True not in dirs_posibles:
                    # Desmarcar camino actual a medida que se retrocede
                    self._getCelda(from_i, from_j).caminoActual = False
                    from_i, from_j = camino_actual.pop()

                    self.setPosicionRata(from_i, from_j)
                    self._redibujar()
                    dirs_posibles = self._direccionesPosibles()

                if from_i == i_inicial and from_j == j_inicial and True not in dirs_posibles:
                    imposible = True

        return resuelto

    def _enLaberinto(self, i, j):
        """Chequear si (i,j) corresponden a coordenadas que caen dentro del laberinto"""
        return i <= self._dim_i - 1 and j <= self._dim_j - 1 and i >= 0 and j >= 0

    def _getCelda(self, i, j):
        """Devolver la celda en (i,j). No se, me resulta mas idiomatico"""
        return self._filas[i][j]

    def _parseCelda(self, *points):
        """Convertir una lista de strings 0 y 1 a tupla de bool"""
        return tuple(pared_str == "1" for pared_str in points)

    def _redibujar(self):
        if self.parent is not None:
            self.parent.update()

# Estas funciones las puse afuera porque considero que no son propiedades de un Laberinto()
# per se, si no mini calculos.


def aIzquierda(i, j):
    return i, j - 1


def aDerecha(i, j):
    return i, j + 1


def aArriba(i, j):
    return i - 1, j


def aAbajo(i, j):
    return i + 1, j


def esIzquierda(from_i, from_j, to_i, to_j):
    return from_j < to_j


def esDerecha(from_i, from_j, to_i, to_j):
    return from_j > to_j


def esArriba(from_i, from_j, to_i, to_j):
    return from_i < to_i


def esAbajo(from_i, from_j, to_i, to_j):
    return from_i > to_i
