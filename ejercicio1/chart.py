import matplotlib
import matplotlib.pyplot as plt
matplotlib.rcParams.update({"errorbar.capsize": 2})

methods = [("Brute", "red"), ("DyC python", "blue"), ("DyC up", "green"), ("DyC merge", "orange")]

data = open("resultados.txt").read()
data = data.split("\n")
data = [x.split(",") for x in data]

fig, ax = plt.subplots()
for method, color in methods:
    d = [x for x in data if x[0] == method]
    print(data)
    ax.errorbar([float(x[1]) for x in d], [float(x[2]) for x in d], yerr=[float(x[3]) for x in d], ecolor="#555555", color=color, label=method)

ax.set(xlabel="n", ylabel="Tiempo (s)",
       title="Comparación de algoritmos")
ax.grid()
ax.legend()
plt.savefig("ej1.pdf")
plt.show()
