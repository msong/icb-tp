import math
import random
import time
import sys
sys.setrecursionlimit(99999)


def listaDePuntos(fn: str) -> list:
    """Abre el archivo en el path fn. El archivo tiene que tener puntos en cada linea,
    con las coordenadas de cada dimension separadas por espacio."""
    points = []
    with open(fn) as f:
        for line in f:
            if line:
                points.append(tuple(float(x) for x in line.split(" ")))

    return points


def crearListaDePuntos(n: int, dims=2) -> list:
    """Crea una lista de puntos al azar."""
    points = []
    while len(points) < n:
        p = (random.random() * 100, random.random() * 100)
        points.append(p)

    return points


def distancia(p1: tuple, p2: tuple) -> float:
    """Devuelve la distancia entre dos puntos p1 y p2. Admite N dimensiones."""
    return math.sqrt(sum((dim1 - dim2)**2 for dim1, dim2 in zip(p1, p2)))


def distanciaMinima(l: list) -> tuple:
    """Devuelve el par de puntos de una lista l que posean la minima distancia entre si, por fuerza bruta.
    Requiere al menos dos puntos."""
    return distanciaMinimaRe(l, distancia(l[0], l[1]), min_pair=(l[:2]))


def distanciaMinimaRe(l: list, min_dist: float, i1=0, i2=0, min_pair=()) -> tuple:
    if i1 == len(l) - 1 and i2 == len(l) - 1:
        return min_pair

    pair = (l[i1], l[i2])
    pair_dist = distancia(*pair)
    if pair_dist < min_dist and i1 != i2:
        min_pair = pair
        min_dist = pair_dist

    if i2 == len(l) - 1:
        i1 += 1
        i2 = 0
    else:
        i2 += 1

    return distanciaMinimaRe(l, min_dist, i1, i2, min_pair)


def upsort(l):
    """Devuelve una lista ordenada a partir de la lista de tuplas l utilizando upsort."""
    return upsortRe(l, len(l) - 1, len(l) - 1)


def upsortRe(l, i, m):
    if i == 0:
        return l
    m = maxPos(l, i)
    l[m], l[i] = l[i], l[m]
    return upsortRe(l, i - 1, m)


def maxPos(l, i, m_i=(0, 0), m=(0, 0)):
    if i < 0:
        return m_i
    return maxPos(l, i - 1, i if l[i][0] > m[0] else m_i, l[i] if l[i][0] > m[0] else m)


def mergeSort(l):
    """Devuelve una lista ordenada a partir de la lista de tuplas l utilizando merge sort, por el primer valor de la tupla."""
    if len(l) == 1 or len(l) == 0:
        return l

    mid = len(l) // 2
    left = mergeSort(l[:mid])
    right = mergeSort(l[mid:])
    return merge(left, right)


def merge(l, r, res=[]):
    """Recibe dos listas de tuplas ordenadas l y r, devuelve una lista ordenada res."""
    if len(l) == 0:
        return res + r
    if len(r) == 0:
        return res + l

    if l[0] <= r[0]:
        return merge(l[1:], r, res + [l[0]])
    else:
        return merge(l, r[1:], res + [r[0]])


def distanciaMinimaDyC(l: list, algoritmo="python") -> tuple:
    """Devuelve el par de puntos de una lista l que posean la minima distancia entre si, por divide and conquer."""
    if len(l) == 2:
        # Esos dos puntos, no hay otra posibilidad
        return distancia(*l)

    if algoritmo == "merge":
        l = mergeSort(l)
    elif algoritmo == "up":
        l = upsort(l)
    elif algoritmo == "python":
        l = sorted(l, key=lambda x: x[0])

    return distanciaMinimaDivide(l)


def distanciaMinimaDivide(l: list) -> list:
    if len(l) <= 3:
        return distanciaMinima(l)

    mid = len(l) // 2
    left = distanciaMinimaDivide(l[:mid])
    right = distanciaMinimaDivide(l[mid:])

    d = left if distancia(*left) < distancia(*right) else right
    d_dist = distancia(*d)

    # Banda de +/- d_dist en el medio.
    band = []
    for i in range(len(l)):
        if abs(l[i][0] - l[len(l) // 2][0]) < d_dist:
            band.append(l[i])

    if len(band) > 1:
        # NOTA: Notarás que acá hay otro distanciaMinima().
        # Si en band_min uso distanciaMinimaDivide(), es decir otra llamada recursiva más para encontrarlo,
        # funciona pero despues de un par de veces crashea, supongo que por limitaciones de la recursión en Python.
        # OK 0.7104899690187715 (0.00781 segs) == 0.7104899690187715 (0.00145 segs)
        # OK 1.1308482547330483 (0.00797 segs) == 1.1308482547330483 (0.00162 segs)
        # fish: “python ej1.py” terminated by signal SIGSEGV (Address boundary error)

        band_min = distanciaMinima(band)
        band_min_dist = distancia(*band_min)
        # De todos modos hay una mejora sustancial de los tiempos dado que band siempre va a ser una porción más chica del problema original.

        # Pero no se si la complejidad temporal del >>peor caso<< volvería a ser O(n^2), o tampoco cual sería ese peor caso,
        # y en caso de existir puede que sea una ocurrencia muy rara que jamás me tocó con randoms.
        # Creo que si fuese así se vería en una aumentada dispersión en el gráfico, cosa que no sucede.

        return d if d_dist < band_min_dist else band_min
    else:
        return d


if __name__ == "__main__":
    for _ in range(100):
        for n in range(3, 80):
            for sort in ("python", "up", "merge"):
                l = crearListaDePuntos(n)
                s = time.time()
                brute_pairs = distanciaMinima(l)
                brute_time = time.time() - s
                brute_dist = distancia(*brute_pairs)

                s = time.time()
                dc_pairs = distanciaMinimaDyC(l, algoritmo=sort)
                dc_time = time.time() - s
                dc_dist = distancia(*dc_pairs)

                if brute_dist != dc_dist:
                    print(brute_pairs, brute_dist)
                    print(dc_pairs, dc_dist)
                    raise ValueError("OTRA VEZ MAL CAPO")
                else:
                    print(f"OK {brute_dist} ({brute_time:.5f} segs) == {dc_dist} ({dc_time:.5f} segs)")
