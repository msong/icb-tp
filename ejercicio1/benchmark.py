import time
from statistics import mean, stdev
from ej1 import crearListaDePuntos, distanciaMinima, distanciaMinimaDyC

RUNS = 100


def benchmark():
    sorts = ["python", "up", "merge"]
    with open("resultados.txt", "w+") as f:
        for sort in sorts:
            for n in range(2, 100):
                l = crearListaDePuntos(n)
                times = []
                for _ in range(RUNS):
                    s = time.time()
                    distanciaMinimaDyC(l, algoritmo=sort)
                    elapsed = time.time() - s
                    times.append(elapsed)
                out = f"DyC {sort}, {n}, {mean(times)}, {stdev(times)}\n"

                print(out)
                f.write(out)

        for n in range(2, 100):
            l = crearListaDePuntos(n)
            times = []
            for _ in range(RUNS):
                s = time.time()
                distanciaMinima(l)
                elapsed = time.time() - s
                times.append(elapsed)
            out = f"Brute, {n}, {mean(times)}, {stdev(times)}\n"

            print(out)
            f.write(out)


if __name__ == "__main__":
    benchmark()
