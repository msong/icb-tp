import sys
from PIL import Image
import time
import imageio
import imfilters

Image.MAX_IMAGE_PIXELS = None

file, runs, *_ = sys.argv[1:]
for _ in range(int(runs)):
    img = imageio.imread(file)
    s = time.time()
    imfilters.blur_filter(imfilters.gray_filter(img))
    print(time.time() - s)
