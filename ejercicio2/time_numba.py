import sys
from PIL import Image
import time
import imageio
import imfilters
from ej2 import blur_filter_numba, gray_filter_numba

Image.MAX_IMAGE_PIXELS = None

file, runs, *_ = sys.argv[1:]
for _ in range(int(runs)):
    img = imageio.imread(file)
    s = time.time()
    blur_filter_numba(gray_filter_numba(img))
    print(time.time() - s)
