import numpy as np
from numba import njit, prange, config

import imageio
from PIL import Image

import imfilters


# PIL.Image.DecompressionBombError: cree que esa imagen de 205 megapixeles es un denial of service para llenar la RAM, que ternura.
Image.MAX_IMAGE_PIXELS = None


def gray_filter(img):
    dim_x, dim_y = len(img), len(img[0])
    img_out = np.zeros(shape=(dim_x, dim_y))

    for i in range(dim_x):
        for j in range(dim_y):
            r, g, b = img[i][j]
            color = 0.3 * r + 0.6 * g + 0.11 * b
            img_out[i][j] = color if color <= 255 else 255
            # Si el resultado se pasa de 255, aplanar en 255
            # Esto es porque vi que si la imagen original tiene pixeles en blanco puro 255
            # el resultado de la ecuacion es 257.55, que en enteros 8 bit se excede y vuelve a 0, negro puro en la imagen final.
    return img_out


def blur_filter(img):
    dim_x, dim_y = len(img), len(img[0])
    img_out = np.zeros(shape=(dim_x, dim_y))

    for i in range(dim_x):
        for j in range(dim_y):
            if 0 < i < dim_x - 1 and 0 < j < dim_y - 1:
                img_out[i][j] = (img[i - 1][j] + img[i + 1][j] + img[i][j - 1] + img[i][j + 1]) / 4
            else:
                # Valor que toma en los bordes, como se solicita.
                img_out[i][j] = 0
    return img_out


@njit(parallel=True)
def gray_filter_numba(img):
    dim_x, dim_y = len(img), len(img[0])
    img_out = np.zeros(shape=(dim_x, dim_y))

    for i in prange(dim_x):
        for j in prange(dim_y):
            r, g, b = img[i][j]
            color = 0.3 * r + 0.6 * g + 0.11 * b
            img_out[i][j] = color if color <= 255 else 255
    return img_out


@njit(parallel=True)
def blur_filter_numba(img):
    dim_x, dim_y = len(img), len(img[0])
    img_out = np.zeros(shape=(dim_x, dim_y))

    for i in prange(dim_x):
        for j in prange(dim_y):
            if 0 < i < dim_x - 1 and 0 < j < dim_y - 1:
                img_out[i][j] = (img[i - 1][j] + img[i + 1][j] + img[i][j - 1] + img[i][j + 1]) / 4
            else:
                img_out[i][j] = 0
    return img_out


def save_img(img, path):
    imageio.imwrite(path, img.astype(np.uint8))


if __name__ == "__main__":
    img = imageio.imread("doge.jpg")

    # Vanilla
    save_img(blur_filter(gray_filter(img)), f"out/doge_py.png")

    # Numba
    save_img(blur_filter_numba(gray_filter_numba(img)), f"out/doge_numba.png")

    # C++
    save_img(imfilters.blur_filter(imfilters.gray_filter(img)), f"out/doge_cpp.png")

    # Diff vanilla vs C++
    doge_diff = gray_filter(img) != imfilters.gray_filter(img)
    doge_diff = (doge_diff).astype(np.uint8) * 255
    save_img(doge_diff, f"out/doge_diff.png")
