import statistics
import time
from ej2 import blur_filter, gray_filter, blur_filter_numba, gray_filter_numba
from numba import config
import imageio
import subprocess

MAX_THREADS = 16


def benchmark(file, name, gray_func, blur_func, threads, reps):
    times = []
    for _ in range(reps):
        img = imageio.imread(file)
        s = time.time()
        blur_func(gray_func(img))
        times.append(time.time() - s)
    return file, name, threads, times


def stats_to_str(file, name, threads, times):
    avg = statistics.mean(times)
    stdev = statistics.stdev(times)

    out = f"{file},{name},{threads},{avg},{stdev}\n"

    print(out, end="")
    return out


def run_benchmark(files):
    with open("resultados.txt", "a") as f:
        for file in files:
            reps = 30

            # Los llamo externamente porque no encontre manera de setear el env OMP_NUM_THREADS y NUMBA_NUM_THREADS al paso.
            # En ambos casos solo le da bola on-import.
            for threads in range(1, MAX_THREADS + 1):
                r = subprocess.run(["python", "time_numba.py", file, str(reps)], stdout=subprocess.PIPE, env={"NUMBA_NUM_THREADS": str(threads)})
                times = [float(x) for x in r.stdout.split(b"\n") if x]
                f.write(stats_to_str(file, "numba", threads, times))

            for threads in range(1, MAX_THREADS + 1):
                r = subprocess.run(["python", "time_cpp.py", file, str(reps)], stdout=subprocess.PIPE, env={"OMP_NUM_THREADS": str(threads)})
                times = [float(x) for x in r.stdout.split(b"\n") if x]
                f.write(stats_to_str(file, "cpp", threads, times))

            b = benchmark(file, "py", gray_filter, blur_filter, 1, reps=3)
            f.write(stats_to_str(*b))


if __name__ == "__main__":
    run_benchmark(["doge.jpg", "arte.jpg", "hasselblad.jpg"])
