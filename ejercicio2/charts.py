import collections
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
matplotlib.rcParams.update({"errorbar.capsize": 2})

Run = collections.namedtuple("Run", "file name cores avg stdev")
runs = []


def filter_data(name, file):
    x = [x.cores for x in runs if x.name == name and x.file == file]
    y = [x.avg for x in runs if x.name == name and x.file == file]
    errs = [x.stdev for x in runs if x.name == name and x.file == file]
    return x, y, errs


with open("resultados.txt") as f:
    for line in f:
        if line:
            file, name, cores, avg, stdev = line.split(",")
            cores = int(cores)
            avg = float(avg)
            stdev = float(stdev)
            runs.append(Run(file, name, cores, avg, stdev))


fig, (ax1, ax2, ax3) = plt.subplots(1, 3, sharex=True, sharey=False, figsize=(12, 4))
for name, mp, ax in [("doge.jpg", "0.5", ax1), ("arte.jpg", "1.8", ax2), ("hasselblad.jpg", "205", ax3)]:

    x, y, err = filter_data("numba", name)
    ax.errorbar(x, y, yerr=err, ecolor="#555555", label="Python Numba", marker=".", linestyle=":", color="#1ea6d4")

    x, y, err = filter_data("cpp", name)
    ax.errorbar(x, y, yerr=err, ecolor="#555555", label="C++ OpenMP", marker=".", linestyle=":", color="#60a873")

    ax.set_title(f"{mp} megapixeles")

    bottom, top = ax.get_ylim()
    ax.set_ylim(bottom, top * 1.3)
    ax.grid()
    ax.legend()

fig.text(0.5, 0.04, "Threads", ha="center", va="center")
fig.text(0.05, 0.5, "Tiempo (s)", ha="center", va="center", rotation=90)

plt.savefig("numba-cpp.pdf")

width = 0.03
fig, ax = plt.subplots(figsize=(12, 4))

bars = []
for i, name, mp in [(1, "doge.jpg", "0.5"), (2, "arte.jpg", "1.8"), (3, "hasselblad.jpg", "205")]:
    x, y, err = filter_data("py", name)
    bars.append(ax.bar(width * 4 * i + 1 * width, y[0], width, align="center", bottom=0, yerr=err[0], capsize=3, color="#0062ff"))
    x, y, err = filter_data("numba", name)
    bars.append(ax.bar(width * 4 * i + 2 * width, y[0], width, align="center", bottom=0, yerr=err[0], capsize=3, color="#1ea6d4"))
    x, y, err = filter_data("cpp", name)
    bars.append(ax.bar(width * 4 * i + 3 * width, y[0], width, align="center", bottom=0, yerr=err[0], capsize=3, color="#60a873"))

ax.legend(bars, ["Python", "Python Numba", "C++ OpenMP"])

ax.set_xticklabels([])
plt.ylabel("Tiempo (s)")
fig.text(0.26, 0.06, "0.5MP", ha="center", va="center")
fig.text(0.51, 0.06, "1.8MP", ha="center", va="center")
fig.text(0.77, 0.06, "205MP", ha="center", va="center")
plt.xlabel("")

plt.yscale("log")
plt.savefig("barras.pdf")
