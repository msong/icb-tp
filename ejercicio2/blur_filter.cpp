#include "filters.h"
#include <omp.h>

void blur_filter(float * im_res, float * im, int ii, int jj)
{
    #pragma omp parallel for collapse(2)
    for (auto i=0; i<ii; i++){
        for (auto j=0; j<jj; j++){
            if ((i > 0) && (i < ii - 1) && (j > 0) && (j < jj - 1)) {
                im_res[i*jj+j] = (im[i*jj+j-1] + im[i*jj+j+1] + im[(i-1)*jj+j] + im[(i+1)*jj+j])/4;
            } else {
                im_res[i*jj+j] = 0;
            }
        }
    }
}
