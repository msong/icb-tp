#include "filters.h"
#include <omp.h>

void gray_filter(float * gray, pixel_t * img, int ii, int jj)
{
    #pragma omp parallel for collapse(2)
    for (auto i=0; i<ii; i++){
        for (auto j=0; j<jj; j++){
            int pixel = i*jj+j;
            float res = 0.3 * img[pixel].r + 0.6 * img[pixel].g + 0.11 * img[pixel].b;
            gray[pixel] = (res > 255 ? 255 : res);
        }
    }
}
