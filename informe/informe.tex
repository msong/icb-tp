\documentclass[12pt,fleqn]{article}

\usepackage[spanish]{babel}
\usepackage[margin=1in]{geometry} 
\usepackage{amsmath,amsthm,amssymb}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{csquotes}

\def\code#1{\texttt{#1}}

\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\B}{\mathbb{B}}

\begin{document}
\title{%
    \Huge Trabajo Práctico Final \\ \, \\
    \large Introducción a la Computación\\
    \small Facultad de Ciencias Exactas y Naturales, UBA\\
}
\author{Mauro Song \\
\small Grupo 19: \footnotesize Singleplayer Virtualenv}
\date{}
\maketitle

\section{Ejercicio 1}
El objetivo de este ejercicio fue dada una lista de puntos en dos dimensiones, obtener el par de puntos que posee la mínima distancia. El método más básico, el de fuerza bruta, consiste en comparar todos los puntos contra todos, excepto por si mismo, y esto resulta en una complejidad temporal de $O(n^2)$.
\begin{figure}[!b]
	\centering
	\includegraphics[width=5in]{img/ej1.pdf}
    \caption{Tiempo en función del tamaño de la lista de puntos, entre los distintos métodos utilizados para la búsqueda de la mínima distancia y para el preprocesamiento.}
    \label{ej1}
\end{figure}
Esto puede mejorarse utilizando la técnica de divide and conquer, donde primero hay que preprocesar los datos ordenándolos por su coordenada x. Para esto, se implementó merge sort y upsort, que poseen complejidades temporales de $O(n\, \text{log}\, n)$ y $O(n^2)$ respectivamente, además de compararlos con la implementación list.sort()/sorted() de Python que es $O(n\, \text{log}\, n)$.

En cuanto a las decisiones de diseño del método divide and conquer, en el paso divide se utilizó como caso base un tamaño de lista de 3 puntos o menos, dado que la lista de entrada puede ser tanto con cantidad par o impar de puntos.

Luego de dividir en dos mitades, se guarda el mínimo par de puntos entre izquierda y derecha. Este par va a definir el par de puntos de mínima distancia $D$ que se halló en los cuadrantes que va a actuar como cota, ya que todavía resta buscar si existe algún par de puntos en lados opuestos al corte que cumpla con ser la menor distancia global. Una vez definida la banda por $+D_{dist}$ y $-D_{dist}$ respecto al valor del medio, entre los puntos que quedan delimitados dentro de esta área se busca el par de menor distancia, y se compara con $D_{dist}$. Si es menor, se devuelve el nuevo, si no se devuelve el hallado previamente, $D$.

Se dice que éste método por divide and conquer tiene una complejidad temporal de $O(n\, \text{log}^2\, n)$. Si se observa la figura \ref{ej1}, esto parece cumplirse a juzgar por las pendientes de las curvas y la mejora en la performance signiticativa.

Respecto a los distintos algoritmos de ordenamiento utilizados, este paso de preprocesamiento no parece ser de mucho peso respecto a los tiempos de ejecución observados, ya que no se observa mucha divergencia entre los resultados. Si bien, como era de esperar por ser $O(n^2)$, el algoritmo upsort fue el más lento. Por otro lado tanto merge sort como la implementación utilizada por Python que son $O(n\, \text{log}\, n)$ producieron tiempos comparables.




\section{Ejercicio 2}
\subsection{Consideraciones}
Si bien fue eliminado del enunciado, incluí en este ejercicio la parte en C++ y además OpenMP, porque era más interesante tener algo contra que comparar Numba. No tuve inconvenientes con el módulo para Python en C++ provisto en los archivos base del TP, compila perfectamente sin ninguna modificación, al menos en Linux.

\subsection{Metodología}
Se hizo el archivo benchmark.py para medir tiempos, y un par de scripts que corren individualmente las funciones de Numba y C++ (time\_numba.py y time\_cpp.py), devolviendo a stdout los tiempos individuales de realizar la cantidad especificada de runs. 
Esto último fue debido a que no se pueden setear las variables de entorno al paso, ya que se cargan solo al momento de importar tanto Numba como imfilters, y cualquier modificación posterior no se tiene en cuenta.

El benchmark se corrió con Xorg deshabilitado y desde tty1, para no sumar dispersión a los resultados debido al uso de CPU por parte de aplicaciones de escritorio, es decir para no matar el tiempo con 80 pestañas de Chrome abiertas.


Para este ejercicio, se compararon los métodos con imágenes de 0.5, 1.8 y 205 megapíxeles.

\subsection{Comparación de imágenes}
Las imágenes resultantes se muestran en la figura \ref{comparativa}. En un primer vistazo, las tres imágenes resultado se podría decir que son cualitativamente iguales. De existir alguna diferencia entre imágenes esta debería ser producto de los cálculos, ya que los tres métodos realizan la carga y guardado por el mismo método con imageio (PIL/Pillow). Por lo tanto, no se podría atribuir a diferentes formas de escribir el archivo de salida (en este caso PNG), como por ejemplo con headers o metadatos diferentes.


\begin{figure}[hbtp]
	\centering
	\includegraphics[width=4in]{img/comparativa.pdf}
    \caption{Comparativa entre las imagenes producidas por las tres implementaciones. A simple vista los resultados son comparables tanto en la coloración como en el desenfoque aplicado.}
    \label{comparativa}
\end{figure}

En cuanto a los hashes SHA256 para los tres archivos, se obtiene:
\begin{verbatim}
    bdf52558f13e7db3e3c7059bf4253819ce9bcd0d18f8f529222c1fb9949f6967  doge_py.png
    bdf52558f13e7db3e3c7059bf4253819ce9bcd0d18f8f529222c1fb9949f6967  doge_numba.png
    15a4d5a15ab5f9d8fa2a4a61de9fa76b857e9449ab9e03b0bc98be4e8fe68ba8  doge_cpp.png
\end{verbatim}

Entonces, se puede decir que las imágenes producidas por los filtros implementados en Python puro y Numba son (con mucha probabilidad) idénticas, pero la de C++ presenta diferencias.

En una inspección manual de la imágen producida por la implementación de C++ se observa que algunos de los píxeles presentan desviaciones en el color de 1 unidad (enteros 8 bits) si se la compara con la salida del método con Python o Numba, ver figura \ref{diff}. Estas discrepancias pueden deberse a cómo se redondean los números posteriormente a realizar los cálculos con floats.

\begin{figure}
	\centering
	\includegraphics[width=4in]{img/doge_diff.png}
    \caption{Píxeles diferentes entre la imágen producida por Python y C++. Se restaron las dos matrices entre sí, y para los píxeles cuyo valor fuese distinto a 0, se los representó en negro, caso contrario en blanco.}
    \label{diff}
\end{figure}



\subsection{Resultados}

\begin{figure}
    \centering
    \hspace*{-1.1in}
	\includegraphics[width=9in]{img/numba-cpp.pdf}
    \caption{Tiempo promedio en función de la cantidad de threads para Numba y C++. La desviación estándar de C++ está incluida, pero es muy pequeña a comparación.}
    \label{cppnumba}
\end{figure}

\begin{figure}
	\centering
    \hspace*{-1.1in}
	\includegraphics[width=9in]{img/barras.pdf}
    \caption{Tiempos promedio para los tres métodos y las tres imágenes, en escala logarítmica. En el caso de C++ y Numba, los tiempos corresponden a la ejecución con 16 threads.}
    \label{barras}
\end{figure}

En la figura \ref{cppnumba} puede observarse que para imágenes de resoluciones cotidianas, tanto en el método con C++ como Numba no se aprecia una disminución significativa del tiempo de procesamiento a una mayor cantidad de cores, aunque ésta existe si se comparan los valores absolutos.

En el caso de la imagen de 205 megapíxeles (16544x12400) se ve la ley de Amdahl en la práctica, ya que a medida que se incrementa la cantidad de cores la mejora en los tiempos de ejecución es cada vez menor. Otro rasgo destacable de este gráfico es que se observa que para 16 threads, la pendiente de Numba parece ser horizontal, o incluso aumentar, mientras que en C++ todavía sigue siendo negativa. Esto sugiere que la versión hecha en Numba podría poseer más pasos no paralelos, en serie, que C++ y OpenMP.

Tanto para la implementación en Numba como la de C++, con 9 threads siempre se produce un incremento en el tiempo de procesamiento de forma consistente al repetir el benchmark. Desconozco el motivo exacto, pero se corrió en una CPU de 8 procesadores físicos y 16 procesadores lógicos (HyperThreading/SMT) y sospecho que la causa viene por ahí.
Mi hipótesis es que con 9 threads ejecutándose en 8 cores físicos, ese thread extra resulta más un estorbo para la comunicación entre cores físicos, acceso a cache y RAM, o alguna otra cuestión arquitectónica, esto en una mayor medida que la contribución de trabajo que pueda llegar a hacer para disminuir el tiempo total.


En cuanto a comparar los tiempos promedio para las tres imágenes y las tres implementaciones (figura \ref{barras}), se puede observar que tanto la de Numba como C++ resulta en una mejora sustancial en al menos dos órdenes de magnitud respecto al tiempo de ejecución de la implementación de Python en serie.
Además, a mayor tamaño de la imagen se observa que la diferencia entre la performance de Numba y C++ se hace más pequeña, teniendo tiempos de ejecución en el mismo orden de magnitud.

Nada mal para solamente tener que escribir un decorador de tres letras.

\newpage


\section{Ejercicio 3}

\begin{displayquote}
\it ... se perdió la implementación del TAD Laberinto en una actualización fallida de Ubuntu por lo que es necesario reconstruirlo.    
\end{displayquote}
Y si, eso te pasa por usar Ubuntu. Es todo risas y diversión hasta que un día toca tirar \code{apt-get dist-upgrade} y prender fuego todo.
Cuando seas grande vas a descubrir las bondades de las distros rolling-release que al menos suelen romperse en minicuotas... bueno, a veces se parece más a mantener vivo un tamagochi, en dificultad terapia intensiva.

En fin, como soy buen tipo te lo vuelvo a implementar.

\subsection{Decisiones de diseño}
Agregué la clase \code{Celda} para guardar de qué lados existe una pared en sus atributos, y los estados de \code{visitada} y \code{caminoActual}. Esto es más idiomático que andar accediendo a una pared recordando su índice en una lista y a que dirección corresponde. Lo mismo para \code{visitada} y \code{caminoActual}, es más idiomático que sea un atributo que andar modificando un diccionario. Pero como lo que hay hecho de la GUI lo requiere, para eso está la propiedad \code{info}, que devuelve dicho diccionario como se espera.

Vi que eran un patrón muy repetido las transformaciones en coordenadas acerca de qué es izquierda, arriba, derecha y abajo, que son simplemente sumar o restar 1 a una dimensión. Las definí como funciones auxiliares afuera, porque no me parece que sean un elemento de un \code{Laberinto} en si mismo.


Los atributos privados agregados a la clase \code{Laberinto} son los siguientes:
\begin{itemize}
    \item \code{\_dim\_i} y \code{\_dim\_j}: las dimensiones totales del \code{Laberinto} en filas y columnas.
    \item \code{\_filas}: lista de listas con \code{Celda} que componen el \code{Laberinto}.
    \item \code{\_pos\_rata} y \code{\_pos\_queso}: respectivas posiciones actuales como tupla $(i, j)$.
\end{itemize}

Respecto a la carga de los archivos \code{.lab}, me pareció que el código resultante quedaba más prolijo realizarlo con expresiones regulares que con splits.

\subsection{Resolución del laberinto}
En un principio se implementó la función \code{resolver} utilizando recursión, la lógica era igual a como se describe, pero se abandonó esta idea cambiando por su equivalente usando ciclos \code{while} porque con algunos laberintos se superaba el límite del call stack en Python. Específicamente ocurría en el laberinto de 40x40 y 50x50 (éste último con una posición de inicio que se pueda resolver, no la default)

En la historia del repositorio existe un commit antiguo que utiliza recursión.

\subsubsection{Exploración}
Antes de mover la rata, primero se evalua si los movimientos en las cuatro direcciones son posibles, utilizando estas condiciones:

\begin{enumerate}
    \item Que la celda de destino esté dentro de las dimensiones del laberinto.
    \item Que en la celda actual no se interponga una pared hacia esa dirección.
    \item Que la celda destino no haya sido recorrida antes: $\neg$ (\code{visitada} $\lor$ \code{caminoActual})
\end{enumerate}

Si se cumplen las tres entonces es una dirección de movimiento posible para la rata, y mientras de las cuatro direcciones haya al menos una, la rata puede seguir avanzando. El orden de preferencia arbitrario, dado por el orden de los \code{if}, es el siguiente: derecha, abajo, izquierda, arriba.

\subsubsection{Backtracking}
Cuando la rata llega a un callejón sin salida, es decir las cuatro direcciones no son posibles porque se encuentra rodeada por paredes, por celdas que ya fueron visitadas o fuera de los límites del laberinto, se hace el backtracking.

En el backtracking, la rata vuelve por el camino actual que recorrió, desmarcando el flag \code{caminoActual} mientras lo hace, hasta que llega a una celda que posee al menos una dirección posible no explorada, caso en el cual se resume el ciclo normal anteriormente descripto. Si esta situación nunca sucede mientras se vuelve por todo el camino actual, la rata va a terminar regresando a la celda de inicio. Estando en la celda de inicio de nuevo, si además no quedan direcciones disponibles, entonces se concluye que el laberinto no tiene solución.



\newpage
\begin{center}
\Huge ¡Felices fiestas!



\begin{figure}[!b]
    \begin{center}
        \includegraphics[height=3.5in]{img/ff1.jpg}
    
        \vspace{1cm}
    
        \includegraphics[height=4in]{img/ff2.jpg}
       \vspace{0.1cm}
      \end{center}
    \end{figure}

\end{center}
\end{document}
